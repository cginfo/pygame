import pygame

class Screen():
    def __init__(self, caption, width, height, fill):
        self.caption = caption
        self.width = width
        self.height = height
        self.fill = fill
        self.current = False

    def makeCurrent(self):
        pygame.display.set_caption(self.caption)
        self.current = True
        self.screen = pygame.display.set_mode((self.width, self.height))

    def endCurrent(self):
        self.current = False

    def checkUpdate(self):
        return self.current

    def screenUpdate(self):
        if self.current:
            self.screen.fill(self.fill)

    def returnTitle(self):
        return self.screen

class Button():
    def __init__(self, x, y, sx, sy, bcolour, fbcolour, font,  fontsize, fcolour, text):
        self.x = x
        self.y = y
        self.sx = sx
        self.sy = sy
        self.bcolour = bcolour
        self.fbcolour = fbcolour
        self.fontsize = fontsize
        self.fcolour = fcolour
        self.text = text
        self.current = False
        self.buttonf = pygame.font.SysFont(font, fontsize)

    def showButton(self, display):
        if self.current:
            pygame.draw.rect(display, self.fbcolour, (self.x, self.y, self.sx, self.sy))
        else:
            pygame.draw.rect(display, self.bcolour, (self.x, self.y, self.sx, self.sy))

        textsurface = self.buttonf.render(self.text, False, self.fcolour)
        display.blit(textsurface, (self.x + self.sx/2 - self.fontsize/2*len(self.text)/2 , self.y + self.sy/2 - self.fontsize/2 -4))

    def focusCheck(self, mousepos, mouseclick):
        if mousepos[0] >= self.x and mousepos[0] <= self.x + self.sx and mousepos[1] >= self.y and mousepos[1] <= self.y + self.sy:
            self.current = True
            return mouseclick[0]
        else:
            self.current = False
            return False

