import sys
import pygame
import os
#os.chdir("/home/cedric/Travail/AlgoInfo/CodesPython/PyGame/Hanoi/")

from ScreenButton import *

## Classes
class MenuScreen(Screen):
    def __init__(self, title, caption, w, h, fill):
        super().__init__(caption, w, h,fill)
        self.title = title
        self.font = pygame.font.SysFont("liberationsans", 30)
        self.bouton_jeu = Button(50, 150, 150, 50, (140, 70, 0), (140, 70, 0), "liberationsans", 30, (0, 0, 0), "Jeu")
        self.bouton_solution = Button(300, 150, 150, 50, (140, 70, 0), (140, 70, 0), "liberationsans", 30, (0, 0, 0), "Solution")

    def screenUpdate(self):
        if self.current:
            self.screen.fill(self.fill)
            self.screen.blit(self.font.render(self.title, 1, (0, 0, 0)), (250 - 30/2*len(self.title)/2 , 25))
        self.bouton_jeu.showButton(self.returnTitle())
        self.bouton_solution.showButton(self.returnTitle())
        pygame.display.update()


class Disque():
    couleurs = [ (60 + 20*k, 30 + 10*k, 0) for k in range(9)]
    def __init__(self, s, n):
        self.taille = n
        self.largeur = 100 - 10*n
        self.hauteur = 20
        self.couleur = Disque.couleurs[n]
        self.pos = 0
        self.ecran = s

    def affiche(self, tour, rang):
        x = 100 + 150*tour
        y = 250 - self.hauteur*rang
        pygame.draw.polygon(self.ecran, self.couleur, [(x - self.largeur//2, y), (x + self.largeur//2, y), (x + self.largeur//2, y - self.hauteur), (x - self.largeur//2, y - self.hauteur)])

class Tour():
    marron = (40, 20, 0)
    def __init__(self, s, n):
        self.pos = n
        self.disques = []
        self.ecran = s

    def affiche(self):
        pygame.draw.line(self.ecran, Tour.marron, (100 + 150*self.pos, 50), (100 + 150*self.pos, 250), 10)
        pygame.draw.line(self.ecran, Tour.marron, (25 + 150*self.pos, 255), (175 + 150*self.pos, 255), 10)
        for r, d in enumerate(self.disques):
            d.affiche(self.pos, r)

    def ajoute_disque(self, disque):
        self.disques.append(disque)

    def retire_disque(self):
        return self.disques.pop(-1)

    def dep_valide(self, disque):
        return self.disques == [] or self.disques[-1].taille < disque.taille

class Hanoi():
    COULEUR_FOND = (211, 211, 211)
    def __init__(self, s, n, sol, w = 500, h = 300):
        self.nb_disques = n
        self.ecran = s
        self.tours = []
        self.resolution = []
        self.sol = sol
        self.nb_coups = 0
        self.sound = '288.wav'
        self.flag = True
        self.pris = False
        self.myfont = pygame.font.SysFont("liberationsans", 20)

    def resol(self):
        def hanoi(n, pi_dep, pi_arr, pi_inter):
            if n > 1:
                hanoi(n-1, pi_dep, pi_inter, pi_arr)
                hanoi(1, pi_dep, pi_arr, pi_inter)
                hanoi(n-1, pi_inter, pi_arr, pi_dep)
            else:
                deplacements.append((pi_dep, pi_arr))
        deplacements = []
        hanoi(self.nb_disques, 0, 2, 1)
        return deplacements

    def initialisation(self):
        self.flag = True
        self.pris = False
        self.nb_coups = 0
        self.tours = [Tour(self.ecran, k) for k in range(3)]
        self.resolution = self.resol()
        for k in range(self.nb_disques):
            self.tours[0].ajoute_disque(Disque(self.ecran, k))
        self.affiche()

    def affiche(self):
        self.ecran.fill(Hanoi.COULEUR_FOND)
        self.ecran.blit(self.myfont.render('n = ' + str(self.nb_disques), 1, (0, 0, 0)), (25, 25))
        for tour in self.tours:
            tour.affiche()

    def anime_sol(self):
        # pygame.mixer.music.load(self.sound)
        delai = int(self.nb_disques * 1000 / len(self.resolution))
        for dep in self.resolution:
            d = self.tours[dep[0]].retire_disque()
            self.tours[dep[1]].ajoute_disque(d)
            pygame.time.delay(delai)
            self.affiche()
            self.nb_coups += 1
            label = 'déplacements = ' + str(self.nb_coups)
            # pygame.mixer.music.play()
            self.ecran.blit(self.myfont.render(label, 1, (0, 0, 0)), (300, 25))
            pygame.display.flip()

    def lancement(self):
        pygame.display.set_caption("Tours de Hanoï")
        if self.sol:
            self.boucle_evenements_sol()
        else:
            self.boucle_evenements_jeu()

    def boucle_evenements_sol(self):
        self.initialisation()
        done = False
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.initialisation()
                        self.anime_sol()
                    elif event.button == 3:
                        self.initialisation()
                    elif event.button == 4:
                        self.nb_disques = min(9, self.nb_disques + 1)
                        self.initialisation()
                    elif event.button == 5:
                        self.nb_disques = max(1, self.nb_disques - 1)
                        self.initialisation()
            pygame.display.flip()

    def boucle_evenements_jeu(self):
        self.initialisation()
        pygame.mixer.music.load(self.sound)
        done = False
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                elif self.flag and len(self.tours[-1].disques) == self.nb_disques:
                    self.flag = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1 and not self.pris and self.flag:
                        pygame.mixer.music.play()
                        tour = (pygame.mouse.get_pos()[0] - 25) // 150
                        try:
                            d = self.tours[tour].retire_disque()
                            self.pris = not self.pris
                        except:
                            pass
                    elif event.button == 1 and self.pris and self.flag:
                        tour = (pygame.mouse.get_pos()[0] - 25) // 150
                        if self.tours[tour].dep_valide(d):
                            pygame.mixer.music.play()
                            self.tours[tour].ajoute_disque(d)
                            self.pris = not self.pris
                            self.nb_coups += 1
                    elif event.button == 3 and not self.flag:
                        self.initialisation()
                    elif event.button == 4:
                        self.nb_disques = min(9, self.nb_disques + 1)
                        self.initialisation()
                    elif event.button == 5:
                        self.nb_disques = max(1, self.nb_disques - 1)
                        self.initialisation()
                self.affiche()
                label = 'déplacements = ' + str(self.nb_coups)
                self.ecran.blit(self.myfont.render(label, 1, (0, 0, 0)), (300, 25))
            pygame.display.flip()


def echange_ecrans(ancien, nouveau):
    ancien.endCurrent()
    nouveau.makeCurrent()
    nouveau.screenUpdate()

## Programme principal

WIDTH, HEIGHT = 500, 300
COULEUR_FOND = (211, 211, 211)
if __name__ == '__main__':
    pygame.init()
    pygame.font.init()
    pygame.mixer.init()

    ecran_menu = MenuScreen("Tours de Hanoï", "Menu", WIDTH, HEIGHT, COULEUR_FOND)
    ecran_jeu = Screen("Tours de Hanoï", WIDTH, HEIGHT, COULEUR_FOND)
    ecran_solution = Screen("Tours de Hanoï", WIDTH, HEIGHT, COULEUR_FOND)

    ecran_menu.makeCurrent()
    ecran_menu.screenUpdate()

    done = False
    while not done:
        mouse_pos = pygame.mouse.get_pos()
        mouse_click = pygame.mouse.get_pressed()
        if ecran_menu.checkUpdate():
            if ecran_menu.bouton_jeu.focusCheck(mouse_pos, mouse_click):
                echange_ecrans(ecran_menu, ecran_jeu)
                Hanoi(ecran_jeu.returnTitle(), 3, False).lancement()
                echange_ecrans(ecran_jeu, ecran_menu)
            elif ecran_menu.bouton_solution.focusCheck(mouse_pos, mouse_click):
                echange_ecrans(ecran_menu, ecran_solution)
                Hanoi(ecran_solution.returnTitle(), 3, True).lancement()
                echange_ecrans(ecran_solution, ecran_menu)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
        pygame.display.update()

    pygame.quit()
