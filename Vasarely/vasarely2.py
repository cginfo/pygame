import sys
import math
import pygame

## Fonctions
def translation(point, vecteur):
    return tuple(point[k] + vecteur[k] for k in range(len(point)))

def symetrie(point):
    return (point[0], -point[1], point[2])

def dep(point):
    return translation(symetrie(point), VECTEUR)

def rotation(point, centre, angle):
    alpha = - angle * math.pi / 180
    x_c, y_c, z_c = centre
    x_p, y_p, z_p = point
    x = int(x_c + math.cos(alpha)*(x_p - x_c) - math.sin(alpha)*(y_p - y_c))
    y = int(y_c + math.sin(alpha)*(x_p - x_c) + math.cos(alpha)*(y_p - y_c))
    return (x, y, z_c)

def deformation(p, centre, rayon):
    """ Calcul des coordonnées d'un point suite à la déformation engendrée par la
    sphère émergeante
    Entrées :
        p : coordonnées (x, y, z) du point du dalage à tracer (z = 0) AVANT déformation
        centre : coordonnées (X0, Y0, Z0) du centre de la sphère
        rayon : rayon de la sphère
    Sorties : coordonnées (xprim, yprim, zprim) du point du dallage à tracer APRÈS déformation
    """
    x, y, z = p
    xprim, yprim, zprim = x, y, z
    xc, yc, zc = centre
    if rayon**2 > zc**2:
        zc = zc if zc <= 0 else -zc
        r = math.sqrt((x - xc) ** 2 + (y - yc) ** 2) # distance horizontale depuis le point à dessiner jusqu'à l'axe de la sphère
        rayon_emerge = math.sqrt(rayon ** 2 - zc ** 2) # rayon de la partie émergée de la sphère
        rprim = rayon * math.sin(math.acos(-zc / rayon) * r / rayon_emerge)
        if 0 < r <= rayon_emerge: # calcul de la déformation dans les autres cas
            xprim = xc + (x - xc) * rprim / r # les nouvelles coordonnées sont proportionnelles aux anciennes
            yprim = yc + (y - yc) * rprim / r
        if r <= rayon_emerge:
            beta = math.asin(rprim / rayon)
            zprim = zc + rayon * math.cos(beta)
            if centre[2] > 0:
                zprim = -zprim
    # return (int(xprim), int(yprim), int(zprim))
    return (xprim, yprim, zprim)

def hexagone(sc, point, longueur, col, centre, rayon):
    p = translation(point, (longueur, 0, 0))
    sommets = list(map(dep,[deformation(rotation(p, point, 60*i), centre, rayon) for i in range(7)]))
    for i in range(3):
        pygame.draw.polygon(sc, col[i], [dep(deformation(point, centre, rayon))[:2], sommets[2*i][:2], sommets[1+2*i][:2], sommets[2+2*i][:2]])

def pavage(sc, largeur, longueur, col, centre, rayon):
    x, y = -largeur /2 - longueur, -largeur /2
    l = 1
    while y <= largeur /2 + longueur:
        x = -largeur /2 + longueur * (l-1)
        while x <= largeur /2 + longueur:
            hexagone(sc, (x, y, 0), longueur, couleurs, centre, rayon)
            x += 3*longueur
        y += math.sqrt(3) * longueur /2
        l = 3.5 - l

## Initialisation
pygame.init()


## Constantes
width = 610
VECTEUR = (width // 2, width // 2, 0)

size = (width, width)

BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
COULEUR_FOND = (211, 211, 211)
couleurs = [BLUE, BLACK, RED]

CENTRE_CERCLE = (0, 0, 0)
RAYON_CERCLE = 150

## Écran

screen = pygame.display.set_mode(size)
pygame.display.set_caption("Projet Vasarely")
screen.fill(COULEUR_FOND)

## Boucle des événements
done = False

centre_cercle = CENTRE_CERCLE
rayon_cercle = RAYON_CERCLE
while not done:
    pavage(screen,width, 20, couleurs, centre_cercle, rayon_cercle)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                centre_cercle = symetrie(translation((event.pos) + (0,), (-width // 2, -width // 2, 0)))
            elif event.button == 4:
                rayon_cercle = min(rayon_cercle + 10, width/2)
            elif event.button == 5:
                rayon_cercle = max(rayon_cercle - 10, 0)
    pygame.display.flip()


## Fermeture de la fenêtre
pygame.quit()
